# How-to Export Oracle Database

This how-to describes how to export `c10` database from image. Note: the `c10` images won't run on machine more memory e.g. 64GB and will also not run in OpenShift. There are three database images: [ghcr.io/competentnl/cnl-database](https://github.com/users/competentNL/packages/container/package/cnl-database), [ghcr.io/competentnl/vdab-db](https://github.com/users/competentNL/packages/container/package/vdab-db) and [ghcr.io/competentnl/boc-db](https://github.com/users/competentNL/packages/container/package/boc-db).

- [Run container](#run-container)
- [Enter container](#enter-container)
- [Prepare export](#prepare-export)
- [Export database](#export-database)
- [Repeat](#repeat)


## Run container

```bash
ssh io  # my laptop
sudo su - root
mkdir /tmp/export
rm /tmp/export/*
docker run -d -p 1521:1521 -v /tmp/export:/tmp/export --name cnl-db ghcr.io/competentnl/cnl-database:filled-0.9.19
```

## Enter container

Enter the running container

```bash
docker exec -it cnl-db bash
```

## Prepare export

```bash
chown oracle:oracle /tmp/export/ -R
su oracle -c "$CHARSET_MOD $ORACLE_HOME/bin/sqlplus / as sysdba"
CREATE DIRECTORY DUMP_DIR AS '/tmp/export';
GRANT READ, WRITE ON DIRECTORY DUMP_DIR TO SYSTEM;
alter user system identified by secret;
alter user system account unlock;
select username from dba_users;
exit
```

## Export database

```bash
export ORACLE_HOME=/u01/app/oracle/product/12.2.0/SE
$ORACLE_HOME/bin/expdp system/secret schemas=CNL dumpfile=cnl_database%U.dmp directory=DUMP_DIR logfile=cnl.log filesize=1000M EXCLUDE=STATISTICS
```

Note: for VDAB full nodig?
```bash
#$ORACLE_HOME/bin/expdp system/secret schemas=VDAB,POOL_COMPETENT,SCH_COMPETENT dumpfile=vdab%U.dmp directory=DUMP_DIR logfile=vdab.log filesize=1000M EXCLUDE=STATISTICS
$ORACLE_HOME/bin/expdp system/secret full=y dumpfile=vdab%U.dmp directory=DUMP_DIR logfile=vdab.log filesize=1000M EXCLUDE=STATISTICS
```

```bash
$ORACLE_HOME/bin/expdp system/secret schemas=IMPDP,REG_OWNER dumpfile=boc%U.dmp directory=DUMP_DIR logfile=boc.log filesize=1000M
```


## Repeat

Repeat above steps for `ghcr.io/competentnl/vdab-db:1.0.0`, `ghcr.io/competentnl/boc-db:1.0.0`.
