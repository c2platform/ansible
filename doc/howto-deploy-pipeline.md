# How-to Deployment Pipeline

his guide will walk you through the process of setting up a GitLab pipeline to a local development instance of OpenShift. For setting up such an instance see [How-to CodeReady Containers (CRC)](./howto-crc.md). The example project used in this guide is [c2platform/examples/nodejs-openshift](https://gitlab.com/c2platform/examples/nodejs-openshift). This guide is based on [Using GitOps with a Kubernetes cluster](https://docs.gitlab.com/ee/user/clusters/agent/gitops.html) and the example project [GitLab-examples / ops / GitOps demo / k8s-agents - GitLab Agent demo configuration · GitLab](https://gitlab.com/gitlab-examples/ops/gitops-demo/k8s-agents).

- [Design](#design)
- [Container view](#container-view)

## Design



```plantuml
@startuml

participant D as "Developer"
participant A as "Application repository"
participant M as "Manifest repository"
participant K as "GitLab agent"
participant C as "Agent configuration repository"

loop Regularly
K -> C : Grab the configuration
end

D -> A : Pushing code changes
A -> M : Updating manifest
loop Regularly
K -> M : Watching changes
M -> K : Pulling and applying changes
end

@enduml
```





C4 PlantUML extension
content.puml
#  PlantUML extensinop

High level view

```plantuml
@startuml blogging-context
title Blogging Context
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(author, "Author")
System(blog, "Acme Blog")

Rel(author, blog, "Create Post")
Rel(author, blog, "Update Post", "https")

Person_Ext(reader, "Reader")
Rel(reader, blog, "Read Post", "https")
Rel(reader, blog, "Search Post", "https")
Rel(blog, reader, "Notify New Post","https")

@enduml
```

```plantuml
@startuml blogging-context
title Deployment Pipeline
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")
System(c2platform, "c2platform")
Rel(engineer, c2platform, "Push change")
Rel(engineer, c2platform, "Create tag")

@enduml
```

## Container view

```plantuml
@startuml blogging-container
title TODO
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

LAYOUT_WITH_LEGEND()

Person(author, "Author")
Person_Ext(reader, "Reader")

System_Boundary(blog, "Acme Blog") {
    Container(blogService, "Blogging Service", "dropwizard")
    Container(admin, "Admin Frontend", "reactjs")
    Rel(author, admin, "Create Post")
    Rel(author, admin, "Update Post")

    Rel(admin, blogService, "Create Post", "https")
    Rel(admin, blogService, "Update Post")

    ContainerDb(blogDb, "BlogDb", "MySQL")
    Rel(blogService, blogDb, "Insert Post Row", "jdbc")
    Rel(blogService, blogDb, "Update Post Row", "jdbc")
}
@enduml
```

```plantuml
@startuml blogging-container
title TODO
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

LAYOUT_WITH_LEGEND()

Person(author, "Author")
Person_Ext(reader, "Reader")

System_Boundary(blog, "Acme Blog") {
    Container(blogService, "Blogging Service", "dropwizard")
    Container(admin, "Admin Frontend", "reactjs")
    Rel(author, admin, "Create Post")
    Rel(author, admin, "Update Post")

    Rel(admin, blogService, "Create Post", "https")
    Rel(admin, blogService, "Update Post")

    ContainerDb(blogDb, "BlogDb", "MySQL")
    Rel(blogService, blogDb, "Insert Post Row", "jdbc")
    Rel(blogService, blogDb, "Update Post Row", "jdbc")
}
@enduml
```

```plantuml
@startuml

skinparam defaultTextAlignment center

!define ICONURL https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.4.0
!include ICONURL/common.puml
!include ICONURL/font-awesome-5/server.puml
!include ICONURL/font-awesome-5/gitlab.puml
!include ICONURL/font-awesome/gears.puml
!include ICONURL/font-awesome/fire.puml
!include ICONURL/font-awesome/clock_o.puml
!include ICONURL/font-awesome/lock.puml
!include ICONURL/font-awesome/cloud.puml
!include ICONURL/devicons/nginx.puml
!include ICONURL/devicons/mysql.puml
!include ICONURL/devicons/redis.puml
!include ICONURL/devicons/docker.puml
!include ICONURL/devicons/linux.puml

FA_CLOUD(internet,internet,cloud) #White {

}

DEV_LINUX(debian,Linux,node){

	FA_CLOCK_O(crond,crond) #White
	FA_FIRE(iptables,iptables) #White

	DEV_DOCKER(docker,docker,node)  {
		DEV_NGINX(nginx,nginx,node) #White
		DEV_MYSQL(mysql,mysql,node) #White
		DEV_REDIS(redis,redis,node) #White
		FA5_SERVER(nexus,nexus3,node) #White
		FA5_GITLAB(gitlab,gitlab,node) #White
		FA_GEARS(gitlabci,gitlab-ci-runner,node) #White

		FA_LOCK(letsencrypt,letsencrypt-client,node) #White
	}
}

internet ..> iptables : http

iptables ..> nginx : http
nginx ..> nexus : http
nginx ..> gitlab : http
gitlabci ..> gitlab : http
gitlab ..> mysql : tcp/ip
gitlab ..> redis : tcp/ip

crond --> letsencrypt : starts every month

@enduml
```

```plantuml
@startuml
allow_mixing
object "inbound message" as m1
object "XML Splitter" as s1

m1 : <url:https://www.onknows.com/software/svg/icons/laptop-ubuntu.svg>
s1 : <img:SplitterIcon.gif>
m2 : <img:MessageIcon.gif>

m1 -> s1
s1 -> m2
@enduml
```






```plantuml
@startuml

rectangle Internet {
    node gitlab.com{
        component nodejs_project {
            component nodejs_pipeline
            component registry
            component nodejs_git_repo
        }
        component gitlab_com_runner
    }
}
actor engineer
rectangle dev_laptop <<local>> {
    node c2d_awx <<virtualbox>> {
        component awx <<container>>
    }
    node OpenShift <<qemu>> {
        together {
            component nodejs_app <<container>>
        }
    }
    node c2d_gitlab_runner <<lxd>> {
        component gitlab_runner <<service>> {
        }
    }
    engineer --> nodejs_git_repo: 1 push
    nodejs_git_repo --> nodejs_pipeline: 2 trigger
    nodejs_pipeline --> gitlab_com_runner: 3 create image
    gitlab_com_runner --> registry: 4 push image
    nodejs_pipeline --> gitlab_runner: 5 trigger
    gitlab_runner --> awx: 6 trigger
    awx --> OpenShift: 7 provision
}
@enduml
```