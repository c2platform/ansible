# How-to Keycloak

This how-to describes creation and management of the [Keycloak](https://www.keycloak.org/) instance / node `c2d-keycloak1` using [c2platform.mw.keycloak](https://gitlab.com/c2platform/ansible-collection-mw/-/tree/master/roles/keycloak) Ansible role. For this setup play two nodes are involved `c2d-keycloak1` and `c2d-db1`. The node `c2d-db1` is a PostgreSQL server. Keycloak is an Open Source Identity and Access Management solution and can be used for single-sign on, identity brokering, social login etc.

- [Setup](#setup)
  - [Configuration](#configuration)
  - [Secrets](#secrets)
- [Play](#play)
- [Test](#test)

## Setup

```bash
export BOX="c2d-keycloak1 c2d-db1"
export PLAY=setup
vagrant up $BOX
```

Note: This how-to assumes you created `c2d-rproxy1`. See [How-to Reverse Proxy and CA server](./howto-reverse-proxy.md).
if you haven't created `c2d-rproxy1` yet, you can use

```bash
export BOX="c2d-rproxy1 c2d-keycloak1 c2d-db1"
```

See also [89a092e7](https://gitlab.com/c2platform/ansible/-/commit/89a092e7028cdc3fa520022975127fbe19e6a67e).

### Configuration

Two C2 / project variables in [group_vars/all/c2.yml](../group_vars/all/c2.yml).

|Variable              |Description                                                     |
|----------------------|----------------------------------------------------------------|
|`c2_keycloak_bind_port_https`|Used to set Keycloak HTTPS port and configure the reverse proxy `c2d-rproxy1` `2`|
|`c2_database_hostname`|Sets Keycloak database hostname `keycloak_database_host` and is used to configure `/etc/hosts` with database hostname `database` on all nodes in all plays.|

```yaml
c2_keycloak_bind_port_https: 9443
c2_database_hostname: database
```

### Secrets

Keycloak secrets are stored in the vault [secret_vars/development/main.yml](../secret_vars/development/main.yml).

```yaml
keycloak_admin_password: secret
keycloak_database_password: secret
keycloak_database_admin_password: secret
```

## Play

```bash
export BOX="c2d-keycloak1 c2d-db c2d-rproxy1"
export PLAY="plays/setup.yml"
vagrant up $BOX
vagrant provision $BOX  # if vagrant LXD container(s) already exist
```

## Test

1. Navigate to https://c2platform.org/auth/.
2. Select [Administration Console](https://c2platform.org/auth/admin/).
3. Login using `admin` and `secret`.
