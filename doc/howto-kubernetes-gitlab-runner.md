# How-to Install Gitlab Runner on Kubernetes

This how-to describes **Gitlab Runner**  installation in MicroK8s cluster running on `c2d-ks1`. There are two example projects associated with this how-to [Run Robot tests on Kubernetes using GitLab](https://gitlab.com/c2platform/examples/kubernetes/gitlab-robot) and [Build Docker image on Kubernetes using GitLab](https://gitlab.com/c2platform/examples/kubernetes/gitlab-docker-build).

- [Create Kubernetes cluster](#create-kubernetes-cluster)
- [Registration token](#registration-token)
- [Edit `values.yaml`](#edit-valuesyaml)
- [Register GitLab Runner](#register-gitlab-runner)
- [Verify](#verify)
- [Add tags](#add-tags)
- [Execute the pipeline](#execute-the-pipeline)


## Create Kubernetes cluster

Create local Kubernetes cluster on `c2d-ks1`.

* [How-to Kubernetes](https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-kubernetes.md)

## Registration token

Navigate to GitLab project [c2platform/examples/kubernetes/gitlab-robot](https://gitlab.com/c2platform/examples/kubernetes/gitlab-robot) and then **Settings** → **CI/CD** → **Runners** and copy the "registration token". Repeat for all projects

## Edit `values.yaml`

A key step to registering a **GitLab Runner** is to create a `values.yaml` file. See online example / template [values.yaml](https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml). See also [Docker-in-Docker with TLS enabled in Kubernetes](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-in-docker-with-tls-enabled-in-kubernetes) and [values.yaml](./howto-kubernetes-gitlab-runner/values.yaml) in this project. From inside `c2d-ks1` edit the `values.yaml` file

```bash
nano /vagrant/doc/howto-kubernetes-gitlab-runner/values.yaml
```

<details>
  <summary>values.yaml example</summary>

```yml
gitlabUrl: https://gitlab.com/
runnerRegistrationToken: <registration token>
tags: "c2d,kubernetes,microk8s,c2d-ks1,io3,docker"
runners:
  config: |
    [[runners]]
      url = "https://gitlab.com/"
      token = "<registration token>"
      executor = "docker"
      [runners.kubernetes]
        image = "ubuntu:20.04"
        privileged = true
      [[runners.kubernetes.volumes.empty_dir]]
        name = "docker-certs"
        mount_path = "/certs/client"
        medium = "Memory"
```

</details>

## Register GitLab Runner

From inside `c2d-ks1` run

```bash
cd /vagrant/doc/howto-kubernetes-gitlab-runner/
helm3 install --namespace nja gitlab-runner -f values.yaml gitlab/gitlab-runner
```

## Verify

Verify the pod is running using `kubectl` or the Kubernetes Dashboard see [How-to Kubernetes Dashboard](./howto-k8s-dashboard.md) that the pod running and registered successfully.

```bash
kubectl config set-context --current --namespace=nja
kubectl get pods
kubectl logs -f gitlab-runner-...
```

<details>
  <summary>Show me</summary>

```bash
vagrant@c2d-ks1:~$ kubectl config set-context --current --namespace=nja
Context "microk8s" modified.
vagrant@c2d-ks1:~$ kubectl get pods
NAME                             READY   STATUS    RESTARTS   AGE
nj-7ff9d46d57-bp5fd              1/1     Running   0          64m
gitlab-runner-7b8b5b76c7-7zm2k   1/1     Running   0          63m
vagrant@c2d-ks1:~$ kubectl logs -f gitlab-runner-7b8b5b76c7-7zm2k
Registration attempt 1 of 30
Runtime platform                                    arch=amd64 os=linux pid=14 revision=d540b510 version=15.9.1
WARNING: Running in user-mode.
WARNING: The user-mode requires you to manually start builds processing:
WARNING: $ gitlab-runner run
WARNING: Use sudo for system-mode:
WARNING: $ sudo gitlab-runner...

Created missing unique system ID                    system_id=r_QVvM6mNZxsMT
Merging configuration from template file "/configmaps/config.template.toml"
WARNING: Support for registration tokens and runner parameters in the 'register' command has been deprecated in GitLab Runner 15.6 and will be replaced with support for authentication tokens. For more information, see https://gitlab.com/gitlab-org/gitlab/-/issues/380872
Registering runner... succeeded                     runner=GR1348941H6iYssSA
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
Configuration (with the authentication token) was saved in "/home/gitlab-runner/.gitlab-runner/config.toml"
Runtime platform                                    arch=amd64 os=linux pid=7 revision=d540b510 version=15.9.1
Starting multi-runner from /home/gitlab-runner/.gitlab-runner/config.toml...  builds=0
WARNING: Running in user-mode.
WARNING: Use sudo for system-mode:
WARNING: $ sudo gitlab-runner...

Configuration loaded                                builds=0
listen_address not defined, metrics & debug endpoints disabled  builds=0
[session_server].listen_address not defined, session endpoints disabled  builds=0
Initializing executor providers                     builds=0
Checking for jobs... received                       job=3894427599 repo_url=https://gitlab.com/c2platform/examples/nodejs-kubernetes.git runner=nq46x6Yh
Appending trace to coordinator...ok                 code=202 job=3894427599 job-log=0-760 job-status=running runner=nq46x6Yh sent-log=0-759 status=202 Accepted update-interval=1m0s
WARNING: Job failed: command terminated with exit code 2
  duration_s=49.327169521 job=3894427599 project=44071435 runner=nq46x6Yh
Appending trace to coordinator...ok                 code=202 job=3894427599 job-log=0-10413 job-status=running runner=nq46x6Yh sent-log=760-10412 status=202 Accepted update-interval=3s
Updating job...                                     bytesize=10413 checksum=crc32:2d0dbfb7 job=3894427599 runner=nq46x6Yh
Submitting job to coordinator...accepted, but not yet completed  bytesize=10413 checksum=crc32:2d0dbfb7 code=202 job=3894427599 job-status= runner=nq46x6Yh update-interval=1s
Updating job...                                     bytesize=10413 checksum=crc32:2d0dbfb7 job=3894427599 runner=nq46x6Yh
Submitting job to coordinator...ok                  bytesize=10413 checksum=crc32:2d0dbfb7 code=200 job=3894427599 job-status= runner=nq46x6Yh update-interval=0s
WARNING: Failed to process runner                   builds=0 error=command terminated with exit code 2 executor=kubernetes runner=nq46x6Yh
```

</details>

## Add tags

The [values.yaml](./howto-kubernetes-gitlab-runner/values.yaml) has tags but for some reason they don't appear in GitLab. So go to [c2platform/examples/nodejs-kubernetes](https://gitlab.com/c2platform/examples/nodejs-kubernetes) and then **Settings** → **CI/CD** → **Runners** and add tags".

```
c2d,kubernetes,microk8s,c2d-ks1,io3,docker
```

## Execute the pipeline

In the project [c2platform/examples/nodejs-kubernetes](https://gitlab.com/c2platform/examples/nodejs-kubernetes) you should now be able to start the pipeline.
