# How-To OpenShift 4 on Azure

This how-to describes how to perform the quick install of OpenShift 4 on Azure. So this is the "**automated**" or "**installer-provisioned infrastructure**" option, not the "user-provisioned infrastructure option that would give more control. So this instruction is based on [Installing a cluster quickly on Azure](
https://docs.openshift.com/container-platform/4.12/installing/installing_azure/installing-azure-default.html).

- [Configure Azure account](#configure-azure-account)
  - [Azure accounts limits ( skip )](#azure-accounts-limits--skip-)
  - [Public DNS zone](#public-dns-zone)
    - [Delegate](#delegate)


## Configure Azure account

See [Configuring an Azure account - Installing on Azure | Installing | OpenShift Container Platform 4.12](https://docs.openshift.com/container-platform/4.12/installing/installing_azure/installing-azure-account.html#installing-azure-account)

### Azure accounts limits ( skip )

Assumed correct / not limited.

### Public DNS zone

Dedicated public hosted DNS zone is reguired. I am using "existing domain and registrar" option. I want to use a subdomain `ocp.c2platform.org`. This gives me [Tutorial: Host your domain in Azure DNS | Microsoft Learn](https://learn.microsoft.com/en-us/azure/dns/dns-delegate-domain-azure-dns).

Two times [Create DNS zone](https://portal.azure.com/#create/Microsoft.DnsZone-ARM), one for c2platform.org and one for ocp.c2platform.org.

|Property               |Value             |Comment                  |
|-----------------------|------------------|-------------------------|
|Subscription           |**N\*\*\*\*\*\*SEDC**        |                         |
|Resource group         |`c2-dns-group`      |Separate / dedicate group|
|Name                   |`ocp.c2platform.org`|                         |
|Resource group location|**West Europe**       |                         |



|Property               |Value             |Comment                  |
|-----------------------|------------------|-------------------------|
|Subscription           |N**30-SEDC        |                         |
|Resource group         |c2-dns-group      |                         |
|Parent zone subscription|N**30-SEDC        |                         |
|Parent zone            |c2platform.org    |                         |
|Resource group         |                  |                         |
|Name                   |ocp               |                         |

<details>
  <summary>Show me</summary>

![](images/azure-ocp-public-dns.png)

</details>


#### Delegate

Via [DNS Zones ocp.c2platform.org](https://portal.azure.com/#@GroupeCGI.onmicrosoft.com/resource/subscriptions/9c5662d6-ada0-4465-a99a-410c66b69c15/resourceGroups/c2-dns-group/providers/Microsoft.Network/dnszones/ocp.c2platform.org/overview) retrieve the DNS zones. This gave me
1. `ns1-03.azure-dns.com.`
2. `ns2-03.azure-dns.net.`
3. `ns3-03.azure-dns.org.`
4. `ns4-03.azure-dns.info.`

Delegate the domain by configuring the above name servers on the domain.

Verify delegation for example using

https://www.nslookup.io/domains/c2platform.org/dns-records/#cloudflare

