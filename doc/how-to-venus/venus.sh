git config --global credential.helper helper cache && git config --global credential.helper "cache --timeout=3600000"
cd /home/vagrant
git clone https://gitlab.com/c2projects/vns/images/alfresco.git
git clone https://gitlab.com/c2projects/vns/images/drupal.git
git clone https://gitlab.com/c2projects/vns/images/keycloak.git
git clone https://gitlab.com/c2projects/vns/images/odoo.git
git clone https://gitlab.com/c2projects/vns/images/odoo-api.git
git clone https://gitlab.com/c2projects/vns/images/portal.git
cd alfresco/
docker-compose up -d
cd ../keycloak
export DB_PASSWORD=whatever
export SSO_HOST=whatever
docker-compose up -d
cd ../drupal
# drupal/docker-compose.yaml
