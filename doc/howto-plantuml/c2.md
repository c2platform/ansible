


```plantuml
@startuml blogging-container
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
!define ICONURL https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.4.0
!include ICONURL/common.puml
!include ICONURL/font-awesome-5/server.puml
!include ICONURL/font-awesome-5/gitlab.puml
!include ICONURL/font-awesome-5/laptop.puml
!include ICONURL/font-awesome/gears.puml
!include ICONURL/font-awesome/fire.puml
!include ICONURL/font-awesome/clock_o.puml
!include ICONURL/font-awesome/lock.puml
!include ICONURL/font-awesome/cloud.puml
!include ICONURL/devicons/nginx.puml
!include ICONURL/devicons/mysql.puml
!include ICONURL/devicons/redis.puml
!include ICONURL/devicons/docker.puml
!include ICONURL/devicons/linux.puml

LAYOUT_WITH_LEGEND()

Person(engineer, "Engineer")
Person(user, "User")

Boundary(c2platform, "C2 Platform") {
    Boundary(local, "local") {
        Container(ansible_local, "Vagrant / Ansible", "")
        System_Boundary(c2d, "c2platform.org")
        System_Boundary(u2d_local, "u2.nl")
    }
    Boundary(internet, "internet") {
        System_Boundary(c2platform_internet, "C2") {
            top to bottom direction
            Container(gitlab, "Gitlab", "gitlab.com/c2platform")
            Container(galaxy, "Ansible Galaxy", "galaxy.ansible.com/c2platform")
            Container(vagrant_cloud, "Vagrant Cloud", "apps.vagrantup.com")
            Container(azure, "Azure", "azure.com")


        }
    }
}
Boundary(customer, "Customer") {
    Boundary(application, "application", "u2.nl") {
        System_Boundary(u2d, "dev.u2.nl") {
            Container(u2d_app1, "App1", "app.dev.u2.nl")
        }
        System_Boundary(u2t, "tst.u2.nl")
        System_Boundary(u2a, "acc.u2.nl")
        System_Boundary(u2p, "prd.u2.nl")
    }
    Boundary(management, "management", "mgmt.u2.nl") {
        Container(azuredevops, "Azure DevOps", "azuredevops.mgmt.u2.nl")
        Container(nexus, "Nexus", "nexus.mgmt.u2.nl")
        Container(awx, "AWX", "awx.mgmt.u2.nl")

    }

}

Rel(engineer, gitlab, "Update Ansible roles / collections", "")
Rel(gitlab, galaxy, "Update Post")
Rel(engineer, ansible_local, "Provision / mimic / build environments locally")
Rel(ansible_local, u2d_local, "")
Rel(ansible_local, c2d, "")
Rel(engineer, awx, "Configure jobs")
Rel(engineer, azuredevops, "Configure environments")
Rel_Right(awx, azuredevops, "")
Rel_Right(awx, u2d_app1, "ssh")
Rel_Down(user, u2d_app1, "https")

@enduml
```