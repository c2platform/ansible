# How-to Install Gitlab Agent

This how-to describes **Gitlab Agent**  installation in MicroK8s node `c2d-ks1`. The example project associated with this how-to is [c2platform/examples/nodejs-kubernetes](https://gitlab.com/c2platform/examples/nodejs-kubernetes). This how-to is based on [Installing the agent for Kubernetes | GitLab](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html).

- [Create Kubernetes cluster](#create-kubernetes-cluster)
- [Register GitLab agent](#register-gitlab-agent)
- [Install GitLab agent](#install-gitlab-agent)
- [Verify](#verify)


## Create Kubernetes cluster

Create local Kubernetes cluster on `c2d-ks`.

* [How-to Kubernetes](https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-kubernetes.md)
* [How-to Kubernetes Dashboard](https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-k8s-dashboard.md) ( optional but recommended)

## Register GitLab agent

First step to create an agent is to create agent file. This has been done see `.gitlab/agents/c2d-mk8s/config.yaml` in [c2platform/examples/kubernetes/gitlab-gitops](https://gitlab.com/c2platform/examples/kubernetes/gitlab-gitops). With `config.yaml` created and pushed to the repository, the agent can be registered. In Gitlab.com navigate to **Infrastructure** → [Kubernetes cluster](https://gitlab.com/c2platform/examples/nodejs-kubernetes/-/clusters) and select **Connect a cluster**. Select the agent ( `c2d-mk8s` in our example ) and register it. This will output some `helm` commands that we need to execute to install the agent in OpenShift.

See [Installing the agent for Kubernetes | GitLab](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html) for more information.

## Install GitLab agent

The agent is then installed by running commands similar to below. Note: the exact command and the token are shown when you register the agent. Note: for MicroK8s the `helm` command is `microk8s helm` but in `c2d-ks` an alias is created for `helm` so that you can use `helm`.

```bash
helm repo add gitlab https://charts.gitlab.io
helm repo update
export GITLAB_AGENT_TOKEN=<token>
export GITLAB_AGENT_FOLDER=c2d-mk8s
kubectl config set-context --current --namespace=gitlab-agent-$GITLAB_AGENT_FOLDER
helm upgrade --install $GITLAB_AGENT_FOLDER gitlab/gitlab-agent \
    --namespace gitlab-agent-$GITLAB_AGENT_FOLDER \
    --create-namespace \
    --set image.tag=v15.9.0 \
    --set config.token=$GITLAB_AGENT_TOKEN \
    --set config.kasAddress=wss://kas.gitlab.com
```

<details>
  <summary>Show me</summary>

```bash
vagrant@c2d-ks1:/tmp$ helm repo add gitlab https://charts.gitlab.io
helm repo update
"gitlab" has been added to your repositories
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "gitlab" chart repository
Update Complete. ⎈Happy Helming!⎈
vagrant@c2d-ks1:/tmp$ export GITLAB_AGENT_TOKEN=jWsjSqySt_jpjzC9xecqxxQD3js7hRcRftz6YuQniNs-F8JM7w
vagrant@c2d-ks1:/tmp$ export GITLAB_AGENT_FOLDER=c2d-mk8s
vagrant@c2d-ks1:/tmp$ kubectl config set-context --current --namespace=gitlab-agent-$GITLAB_AGENT_FOLDER
Context "microk8s" modified.
vagrant@c2d-ks1:/tmp$ helm upgrade --install $GITLAB_AGENT_FOLDER gitlab/gitlab-agent \
    --namespace gitlab-agent-$GITLAB_AGENT_FOLDER \
    --create-namespace \
    --set image.tag=v15.9.0 \
    --set config.token=$GITLAB_AGENT_TOKEN \
    --set config.kasAddress=wss://kas.gitlab.com
Release "c2d-mk8s" does not exist. Installing it now.
NAME: c2d-mk8s
LAST DEPLOYED: Mon Mar  6 07:50:17 2023
NAMESPACE: gitlab-agent-c2d-mk8s
STATUS: deployed
REVISION: 1
TEST SUITE: None

```

## Verify

The `helm upgrade` creates a namespace `gitlab-agent-c2d-mk8s`.

```bash
vagrant@c2d-ks1:/tmp$ kubectl get all --all-namespaces
NAMESPACE               NAME                                            READY   STATUS    RESTARTS       AGE
kube-system             pod/calico-kube-controllers-586dd5cf66-k6cpr    1/1     Running   1 (167m ago)   4d
kube-system             pod/kubernetes-dashboard-dc96f9fc-zgldh         1/1     Running   1 (167m ago)   4d
kube-system             pod/coredns-6f5f9b5d74-n8sq8                    1/1     Running   1 (167m ago)   4d
kube-system             pod/dashboard-metrics-scraper-7bc864c59-bx2n6   1/1     Running   1 (167m ago)   4d
kube-system             pod/calico-node-bmvgb                           1/1     Running   1 (167m ago)   4d
kube-system             pod/metrics-server-6f754f88d-gttst              1/1     Running   1 (167m ago)   4d
gitlab-agent-c2d-mk8s   pod/c2d-mk8s-gitlab-agent-56b9f5bc97-sqq49      1/1     Running   0              4m
nja                     pod/nj-7ff9d46d57-z8z5q                         1/1     Running   0              3m54s

NAMESPACE     NAME                                TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                  AGE
default       service/kubernetes                  ClusterIP   10.152.183.1     <none>        443/TCP                  4d
kube-system   service/metrics-server              ClusterIP   10.152.183.34    <none>        443/TCP                  4d
kube-system   service/dashboard-metrics-scraper   ClusterIP   10.152.183.60    <none>        8000/TCP                 4d
kube-system   service/kube-dns                    ClusterIP   10.152.183.10    <none>        53/UDP,53/TCP,9153/TCP   4d
kube-system   service/kubernetes-dashboard        ClusterIP   10.152.183.213   1.1.4.155     443/TCP                  4d
nja           service/frontend-service            ClusterIP   10.152.183.251   <none>        3000/TCP                 3m54s

NAMESPACE     NAME                         DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kube-system   daemonset.apps/calico-node   1         1         1       1            1           kubernetes.io/os=linux   4d

NAMESPACE               NAME                                        READY   UP-TO-DATE   AVAILABLE   AGE
kube-system             deployment.apps/calico-kube-controllers     1/1     1            1           4d
kube-system             deployment.apps/coredns                     1/1     1            1           4d
kube-system             deployment.apps/kubernetes-dashboard        1/1     1            1           4d
kube-system             deployment.apps/dashboard-metrics-scraper   1/1     1            1           4d
kube-system             deployment.apps/metrics-server              1/1     1            1           4d
gitlab-agent-c2d-mk8s   deployment.apps/c2d-mk8s-gitlab-agent       1/1     1            1           4m
nja                     deployment.apps/nj                          1/1     1            1           3m54s

NAMESPACE               NAME                                                  DESIRED   CURRENT   READY   AGE
kube-system             replicaset.apps/calico-kube-controllers-79568db7f8    0         0         0       4d
kube-system             replicaset.apps/calico-kube-controllers-586dd5cf66    1         1         1       4d
kube-system             replicaset.apps/coredns-6f5f9b5d74                    1         1         1       4d
kube-system             replicaset.apps/kubernetes-dashboard-dc96f9fc         1         1         1       4d
kube-system             replicaset.apps/dashboard-metrics-scraper-7bc864c59   1         1         1       4d
kube-system             replicaset.apps/metrics-server-6f754f88d              1         1         1       4d
gitlab-agent-c2d-mk8s   replicaset.apps/c2d-mk8s-gitlab-agent-56b9f5bc97      1         1         1       4m
nja                     replicaset.apps/nj-7ff9d46d57                         1         1         1       3m54s
```

</details>

Addtionally, if you have the dashboard enabled see [How-to Kubernetes Dashboard](./howto-k8s-dashboard.md) you can use that to check the agent log.


