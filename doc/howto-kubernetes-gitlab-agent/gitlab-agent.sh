helm repo add gitlab https://charts.gitlab.io
helm repo update
export GITLAB_AGENT_TOKEN=<token>
export GITLAB_AGENT_FOLDER=c2d-mk8s
kubectl config set-context --current --namespace=gitlab-agent-$GITLAB_AGENT_FOLDER
helm upgrade --install $GITLAB_AGENT_FOLDER gitlab/gitlab-agent \
    --namespace gitlab-agent-$GITLAB_AGENT_FOLDER \
    --create-namespace \
    --set image.tag=v15.9.0 \
    --set config.token=$GITLAB_AGENT_TOKEN \
    --set config.kasAddress=wss://kas.gitlab.com
