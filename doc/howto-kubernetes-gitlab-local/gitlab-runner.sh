export NAMESPACE=gitlab-runner
export VALUES_YAML=/vagrant/doc/howto-kubernetes-gitlab-local/values.yml
export SECRET=c2-cert
kubectl create namespace $NAMESPACE
kubectl config set-context --current --namespace=$NAMESPACE
kubectl create secret generic $SECRET --namespace $NAMESPACE --from-file=gitlab.c2platform.org.crt=/vagrant/.ca/c2/c2.crt
helm repo add gitlab https://charts.gitlab.io
helm repo update gitlab
# helm search repo -l gitlab/gitlab-runner
helm install --namespace $NAMESPACE gitlab-runner -f $VALUES_YAML gitlab/gitlab-runner
helm upgrade --namespace $NAMESPACE gitlab-runner -f $VALUES_YAML gitlab/gitlab-runner
