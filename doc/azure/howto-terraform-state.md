# How-to Azure Blob Storage for Terraform State

This how-to describes how we use [Azure Blob Storage](https://azure.microsoft.com/en-us/products/storage/blobs/) as a secure backend for the [Terraform State](https://developer.hashicorp.com/terraform/language/state). See also the [azurerm](https://developer.hashicorp.com/terraform/language/settings/backends/azurerm) resource.

- [Create account and container](#create-account-and-container)
- [Backend config](#backend-config)
- [Terraform init](#terraform-init)


## Create account and container

Commands similar to the ones shown below were used to create storage group `c2-terraform`, storage account `c2terraformstate` and storage container `c2-terraform-state-container`.

```bash
export STORAGE_GROUP=c2-terraform
export STORAGE_ACCOUNT=c2terraformstate  # use numbers and lower-case letters only
export STORAGE_CONTAINER=c2-terraform-state-container
az group create --name $STORAGE_GROUP --location westeurope
az storage account create --name $STORAGE_ACCOUNT --resource-group $STORAGE_GROUP --location westeurope --sku Standard_LRS
export STORAGE_ACCOUNT_KEY=$(az storage account keys list --account-name $STORAGE_ACCOUNT --query [0].value --output tsv)
export ARM_ACCESS_KEY=$STORAGE_ACCOUNT_KEY
az storage container create --account-name $STORAGE_ACCOUNT --account-key $STORAGE_ACCOUNT_KEY --name $STORAGE_CONTAINER
```

Note: `ARM_ACCESS_KEY` is the recommended way to provide access to the block storage see [access_key](https://developer.hashicorp.com/terraform/language/settings/backends/azurerm#access_key) and [Configuration Variables](https://developer.hashicorp.com/terraform/language/settings/backends/azurerm#configuration-variables)

> **Warning**: We recommend using environment variables to supply credentials and other sensitive data.

## Backend config

In Terraform project create file `backend.rf` with contents as shown below.

```tf
terraform {
  backend "azurerm" {
    storage_account_name = "c2terraformstate"
    container_name       = "c2-terraform-state-container"
    key                  = "prod.terraform.tfstate"
    # access_key = "abcdefghijklmnopqrstuvwxyz0123456789..."  # don't do this, use ARM_ACCESS_KEY

  }
}
```

## Terraform init

After the creation of `backend.tf` execute `terraform init` to start using this backend.

<details>
  <summary>Show me</summary>

```bash
onknows@io3:~/git/ok/c2/cgi-azure$ terraform init
Initializing modules...

Initializing the backend...
Do you want to copy existing state to the new backend?
  Pre-existing state was found while migrating the previous "local" backend to the
  newly configured "azurerm" backend. No existing state was found in the newly
  configured "azurerm" backend. Do you want to copy this state to the new "azurerm"
  backend? Enter "yes" to copy and "no" to start with an empty state.

  Enter a value: no


Successfully configured the backend "azurerm"! Terraform will automatically
use this backend unless the backend configuration changes.

Initializing provider plugins...
- Reusing previous version of hashicorp/local from the dependency lock file
- Reusing previous version of hashicorp/template from the dependency lock file
- Reusing previous version of hashicorp/azurerm from the dependency lock file
- Using previously-installed hashicorp/local v2.4.0
- Using previously-installed hashicorp/template v2.2.0
- Using previously-installed hashicorp/azurerm v3.47.0

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
onknows@io3:~/git/ok/c2/cgi-azure$
```

</details>