---
markmap:
  colorFreezeLevel: 2
  initialExpandLevel: 1
---

# C1

Highlights are story points 1 2 3 5 8 13

## Software available in dev ==2==

`fmw_12.2.1.4.0_ohs_linux64_Disk1_1of1.zip`
`fmw_12.2.1.4.0_ohs_linux64_Disk1_1of1.zip`
`jdk-8u333-linux-x64.tar.gz`

## Licenses available in dev ==2==

- RHEL maybe
- Weblogic
- JDK
- Developer licenses?

## Images ==2/5==

### CentOS 8 ==2==

### Optional RHEL 8 ==3==

- ==CentOS==
  is preferred

## WLS RHEL8 ==13==

### Prepare OS / install ==6==

#### Environment settings ==1==

- 99-oracle.conf
- sysctl.conf

#### Install Xterm and RHEL packages ==1==

`binutils-2.30-49.0.2.el8` etc

- _Xterm for wizards? ( X11 forwarding )_

#### Mount `/u01` for Oracle ==1==

#### Oracle group and account ==1==

#### Create directories `$ORACLE_HOME` etc ==1==

#### Create install file `/etc/oraInst.loc` ==1==

### JDK ==3==

WLS RHEL 8 jdk8 environment

#### Install JDK ==2==

#### Secure random `java.security` ==1==

### Configure X11 Forwarding ==1==

### Remove `noexec` from `/etc/fstab` ==1==

### Enable crontab for oracle ==1==

### Add `.bash_profile` for oracle ==1==


## OHS ==35==

Based on `INSTALL_0010_***_OHS12.2.1.4_standalone_Generic_v0.2.docx`

#### instal OHS ==5==

Perform manual install using `fmw_12.2.1.4.0_ohs_linux64_Disk1_1of1.zip`, create response file and automate silent install. Note there is an option **Save Response File**.

- *Save Response File*

#### WLS 12c patches ==5==

`opatch lsinventory`

#### Webtier port 80 binding ==2==

#### Standalone configuration ==?==

Using `./config.sh` configure OHS domain.

- *Cannot be automated!?*

#### Post domain settings ==8==

##### Change `nodemanager.properties` ==1==

##### Change  `config.xml` ==1==

##### Change `httpd.conf` ==1==

##### Start nodemanager ==1==

##### Test nodemanager ==1==

##### Start OHS ==1==

##### Test OHS ==1==

##### Create start script nodemanager and OHS ==1==

#### Webtier ==11==

##### Cert ==9==

###### Creation keystore and wallet ==5==

###### Import certificate ==3==

###### Keystore, wallet and certificate in dev ==1==

- *ownca*

##### Configuration `vh_c1.conf`, `ssl.conf` ==2==

#### Web tier testing ==1==

#### Webgate ==3==

##### deployWebGateInstance ==1==

##### Include WebGate ==1==

##### Artifacts OAM server ==1==

- *OAM server?*

#### Log file configuration ==2==

## Oracle ==4==

### Create Oracle 19c database server and instance `c1` ==2==

### Create schema `c1` and `c1rep` ==2==

## WLS ==73==

### Silent install fmw 12.2.1.4 generic ==5==

### WLS 12c patches ==1==

- *see OHS*

### Installation repository ==3==

- *Save Response File*

### Install `c1` domain ==13==

- *Cannot be automated!?*

### Post domain ==25==

#### Change `setUserOverrides.sh` ==2==

#### Create `boot.properties` ==3==

#### Nodemanager activation ==2==

#### Nodemanager start / stop script ==2==

#### WLS configuration

##### Nodemanager machine type ==GUI== ==3==

##### Weblogic plugin enabled ==GUI== ==3==

##### Frontend host clusters ==GUI== ==3==

##### Generate the Boot and Startup properties ==WLST== ==3==

#### Create nodemanager credentials ==WLST== ==3==

#### Create weblogic python scripts ==WLST== ==1==

### Webtier ==2==

#### Creation keystore and wallet ==1==

- *see OHS*

#### Config `vh_c1.conf`, `ssl.conf` ==1==

### Backup ==?==

- *Describes backup of few files*

### C1 domain P/G/P ==24==

#### Keystore identity and trust ==3==

#### Configure keystores in Weblogic ==GUI== ==5==

#### Providers ==13==

Kyndryl and AD?

#### Data sources ==GUI== ==3==

## Notes

1. Commands / configs in RED for OT environments!?
2. WLS manual is 165 pages
3. WLS see **Configuration c1p** copy with highlighted differences of **Configuration c1a** etc `vh_c1.conf`
4. The source for this configuration is the config.xml of the C1 environment running at Kyndryl site.
5. [WebLogic Deploy Tooling](https://github.com/oracle/weblogic-deploy-tooling) *removes the need for hand-coded WLST scripts*
6. [Using the WebLogic Scripting Tool](https://docs.oracle.com/cd/E13222_01/wls/docs90/config_scripting/using_WLST.html)
7. [Oracle WebLogic RESTful Management Services: From Command Line to JavaFX](https://www.oracle.com/technical-resources/articles/middleware/soa-oliveira-wls-rest-javafx.html)
8. [wls-rest-python · PyPI](https://pypi.org/project/wls-rest-python/)
9. +- 130 points / +-25 points per person per sprint