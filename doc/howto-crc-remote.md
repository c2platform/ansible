# How-to Accessing CodeReady Containers ( CRC ) on a Remote Server / PC

Use this guide to access a local CodeReady Containers (CRC) instance remotely via a HAProxy instance on the CRC host. For setting up a local instance of CRC, refer to the [Accessing CodeReady Containers on a Remote Server](https://cloud.redhat.com/blog/accessing-codeready-containers-on-a-remote-server/) guide. This guide is based on [Accessing CodeReady Containers on a Remote Server](https://cloud.redhat.com/blog/accessing-codeready-containers-on-a-remote-server/) article.


```plantuml
@startuml crc-remote
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "")

Boundary(blog, "Ubuntu Laptop", $type=".testing") {
    Container(haproxy, "HAProxy", "")
    Container(crc, "CodeReady Containers ( CRC )", "openshift")
    Rel(engineer, haproxy, "Access OpenShift console", "https")
    Rel(engineer, haproxy, "Access application ", "https")
    Rel(haproxy, crc, "https")

}
@enduml
```


- [Check IP](#check-ip)
- [Provision HAProxy](#provision-haproxy)
- [Configuring DNS for Clients](#configuring-dns-for-clients)

## Check IP

First thing is to check the IP address of CRC. After `crc start` this can for example be found in `/etc/hosts`. In [group_vars/devtop/main.yml](../group_vars/devtop/main.yml) you might have to change `c2_haproxy_crc_ip`.

```yaml
c2_haproxy_crc_ip: 192.168.130.11
```

## Provision HAProxy

To provision HAProxy locally - on the host that is running CRC - you can execute commands below. Note that we are not using `vagrant` here.

```bash
export PLAY=plays/dev/devtop.yml
export INVENTORY=hosts-dev.ini
ansible-playbook $PLAY -i $INVENTORY
```

## Configuring DNS for Clients

Client machines will need to be able to resolve the DNS entries used by CRC. There are various ways to do that:

1. Use `dnsmasq` on your client machine. See [Accessing CodeReady Containers on a Remote Server](https://cloud.redhat.com/blog/accessing-codeready-containers-on-a-remote-server/) on how to that.
2. Edit `/etc/hosts` manually.
3. If you have a more advanced router for example pfSense, you can configure the **DNS Resolver** ( which is also based on `dnsmasq`).
