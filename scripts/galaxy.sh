#!/bin/bash

# Use the environment variable BASE_URL if set, or provide a default value
base_url="${BASE_URL:-https://galaxy.c2platform.org}"

# Function to send a request using curl and capture the token from the cookie
send_request_with_token() {
  local response_headers=$(mktemp)
  local response_body=$(mktemp)

  curl -X "$1" "$2" -H "Content-Type: application/json" -H "Accept: application/json" -d "$3" -c "$response_headers" -o "$response_body"

  echo "Response Headers: $(cat $response_headers)"
  echo "Response Body: $(cat $response_body)"

  # Extract the CSRFToken from the response headers
  local csrf_token
  csrf_token=$(grep -oP '(?<=csrftoken=)[^;]+' "$response_headers")

  if [ -n "$csrf_token" ]; then
    echo "CSRFToken: $csrf_token"
  else
    echo "CSRFToken not found in the response headers."
  fi
}

# Request 1: GET to login and capture CSRFToken
csrf_token=$(send_request_with_token "GET" "$base_url/api/galaxy/_ui/v1/auth/login/" "{}")

# Check if the CSRFToken was obtained successfully
if [ -n "$csrf_token" ]; then
  # Request 2: POST to login with CSRFToken and capture the token
  token=$(send_request_with_token "POST" "$base_url/api/galaxy/_ui/v1/auth/login/" '{"username": "admin", "password": "secret", "csrfmiddlewaretoken": "'"$csrf_token"'"}')

  # Check if the token was obtained successfully
  if [ -n "$token" ]; then
    # Request 3: GET to galaxy with the obtained token
    send_request_with_token "GET" "$base_url/api/galaxy/" "{}"

    # Request 4: GET to a remote with parameters using the obtained token
    send_request_with_token "GET" "$base_url/api/galaxy/pulp/api/v3/remotes/ansible/collection/?name=ivp-tb-geo" "{}"

    # Request 5: POST to create a remote using the obtained token
    send_request_with_token "POST" "$base_url/api/galaxy/pulp/api/v3/remotes/ansible/collection/" '{"requirements_file": "---\ncollections:\n  - community.general\n  - ansible.posix\n  - community.crypto\n  - chocolatey.chocolatey\n  - community.postgresql\n  - ansible.windows\n  - community.windows\n  - c2platform.core\n  - c2platform.mw\n  - c2platform.wincore\n  - c2platform.gis\n  - infra.ah_configuration\n  - awx.awx", "name": "ivp-tb-geo", "url": "https://galaxy.ansible.com/api/", "token": "'"$token"'", "tls_validation": false, "download_concurrency": 10, "max_retries": null, "rate_limit": 8, "signed_only": false, "sync_dependencies": true}'
  else
    echo "Token not found. Authentication failed."
  fi
else
  echo "CSRFToken not found. Authentication failed."
fi
