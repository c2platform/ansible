# How-to Server Update

This how-to describes how server updates are managed in this project using [c2platform.core.server_update](https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/server_update/README.md) role.

- [Yum / Apt cache updates](#yum--apt-cache-updates)
- [Upgrading packages](#upgrading-packages)
- [Test cache update](#test-cache-update)
- [Test upgrade](#test-upgrade)

## Yum / Apt cache updates

In [group_vars/all/main.yml](../group_vars/all/main.yml) has configuration show below. The var `common_manage_server` makes [c2platform.core.server_update](https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/server_update/README.md) part of [c2platform.core.common](https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/common/meta/main.yml) role. The var `server_update_update_cache_enabled` will ensure that Yum or Apt cache is updated in any play where `c2platform.core.common` is included which is the case in most plays for example in the [Keycloak](../plays/mw/keycloak.yml) play.

```yaml
common_manage_server: true
server_update_update_cache_enabled: true
```

## Upgrading packages

Upgrading packages is typically a process over which you want more control so this is done - in this project - as part of a separate, dedicated play [plays/mgmt/server_update.yml](../plays/mgmt/server_update.yml).

## Test cache update

To test cache update and server update for example you can use `c2d-rproxy1` and `2`. Assuming the nodes do not exist:

```bash
export PLAY=plays/setup.yml
export BOX="c2d-rproxy1 cd2-rproxy2"
vagrant up $BOX
```

Note that the logging shows as one of the very first tasks for `c2d-rproxy1`

```
TASK [c2platform.core.server_update : Apt update cache] ************************
changed: [c2d-rproxy1]
```

and for `c2d-rproxy2`

```bash
TASK [c2platform.core.server_update : Yum update cache] ************************
ok: [c2d-rproxy2]
```

## Test upgrade

To test update execute commands below, depending on the situation you might see a restart of the server.

```bash
export PLAY=plays/mgmt/server_update.yml
export BOX="c2d-rproxy1 cd2-rproxy2"
vagrant provision $BOX
```