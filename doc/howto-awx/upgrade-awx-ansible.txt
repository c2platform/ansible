export ANSIBLE_VENV=bkwi
virtualenv /var/lib/awx/venv/$ANSIBLE_VENV -p python3
source /var/lib/awx/venv/$ANSIBLE_VENV/bin/activate
umask 0022
/var/lib/awx/venv/$ANSIBLE_VENV/bin/pip install python-memcached psutil
/var/lib/awx/venv/$ANSIBLE_VENV/bin/pip install -U "ansible == 4.10.0"