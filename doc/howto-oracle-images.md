# How-to Oracle Images

This how-to describes how Oracle database images can be created using GitHub repository [oracle/docker-images](https://github.com/oracle/docker-images).

```bash
git clone git@github.com:oracle/docker-images.git ~/git/github/oracle-docker-images
cd ~/git/github/oracle-docker-images/OracleDatabase/SingleInstance/dockerfiles
./buildContainerImage.sh
export VERSION=12.2.0.1  # 19.3.0
cd $VERSION/
cd ..
./buildContainerImage.sh -v $VERSION -t onknows/oracle-database:$VERSION -e
docker push onknows/oracle-database:$VERSION
```

Two images are made available this way. See [hub.docker.com](https://hub.docker.com/r/onknows/oracle-database/tags).
