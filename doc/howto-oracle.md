# How-to Oracle

This how-to describes how we can create the Oracle database `c2d-oracle1`. See [Oracle play](../plays/mw/oracle.yml) and [group_vars](../group_vars/oracle/main.yml)  configuration. This play uses uses the [c2platform.dev.oracle_database](https://gitlab.com/c2platform/ansible-collection-dev/-/blob/master/roles/oracle_database/README.md) Ansible role that runs a Oracle database in a Docker container.

- [Setup](#setup)
- [Check container](#check-container)
- [Login as sys](#login-as-sys)
- [Login as c2d](#login-as-c2d)
- [Python interface](#python-interface)
- [Schema](#schema)
- [SQL Developer ( optional )](#sql-developer--optional-)

## Setup

```bash
vagrant up c2d-oracle1
```

## Check container

After up the Docker container `oracle` should be running. Execute following commands

```bash
vagrant ssh c2d-oracle1
sudo su -
docker ps
```

<details>
  <summary>Show me</summary>

```bash
[:ansible-dev]└3 master(+7/-0)* 130 ± vagrant ssh c2d-oracle1
Last login: Wed Dec 14 05:51:34 2022 from 1.1.4.1
[vagrant@c2d-oracle1 ~]$ sudo su -
[root@c2d-oracle1 ~]# docker ps
CONTAINER ID   IMAGE                        COMMAND                  CREATED              STATUS                                 PORTS                    NAMES
a3b8bc04aca6   doctorkirk/oracle-19c:19.9   "/bin/sh -c 'exec $O…"   About a minute ago   Up About a minute (health: starting)   0.0.0.0:1521->1521/tcp   oracle
[root@c2d-oracle1 ~]#
```

</details>

## Login as sys

After successful vagrant up you can login as `sys` in the `c2d` instance that has been created.

```bash
vagrant ssh c2d-oracle1
sqlplus sys/secret@c2d as sysdba
```

Notes:

1. the above sqlplus works because `TNS_ADMIN` is set using `/etc/profile.d/oracle.sh` which points to `/home/vagrant/tnsnames.ora`.
2. The `c2d` instance ( and the Oracle database ) is running in a container named `oracle`.

```
c2d = (DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = c2d-oracle1)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = c2d)))
```

## Login as c2d

The play also created schema `c2d` which you should be able to connect to using

```bash
sqlplus c2d/secret@c2d
```

## Python interface

The [python-oracledb](https://oracle.github.io/python-oracledb/) is installed. A test script `test.py` utilizes this library to test connection to the database.

The older [cx_Oracle](https://cx-oracle.readthedocs.io/en/latest/index.html) is also installed. With test script `test2.py`. Both are installed because for [ari_stark.ansible_oracle_modules](https://galaxy.ansible.com/ari_stark/ansible_oracle_modules) collection we need `cx_Oracle`.

```bash
vagrant ssh c2d-oracle1
python3 test.py
python3 test2.py
```

<details>
  <summary>Show me</summary>

Note the line with `(datetime.datetime(2022, 11, 9, 7, 32, 35),)` when we execute `python3 test.py`.

  ```bash
[vagrant@c2d-oracle1 ~]$ python3 test.py
/usr/local/lib64/python3.6/site-packages/oracledb/connection.py:45: CryptographyDeprecationWarning: Python 3.6 is no longer supported by the Python core team. Therefore, support for it is deprecated in cryptography and will be removed in a future release.
  from . import base_impl, thick_impl, thin_impl
(datetime.datetime(2022, 11, 9, 7, 32, 35),)
[vagrant@c2d-oracle1 ~]$
  ```

The second example uses the old

```bash
[vagrant@c2d-oracle1 ~]$ python3 test2.py
(datetime.datetime(2022, 11, 9, 7, 58, 8),)
```

</details>


## Schema

The configuration will also create a schema `c1` and assign some privileges. This is configured in [group_vars/oracle/main.yml](../group_vars/oracle/main.yml) with lists `oracle_users` and `oracle_grants`.

## SQL Developer ( optional )

Of course it also possible to connect to the database from your Vagrant / LXD host. For if you use Oracle SQL Developer you should be able to create the following connections and connect to the database.
I

|Name             |Username|Password|Hostname   |SID  |Role    |
|-----------------|--------|--------|-----------|-----|--------|
|`c1@c2d-oracle1` |`c2d`    |`secret`|`1.1.4.242`|`c2d`|        |
|`sys@c2d-oracle1`|`c2d`    |`secret`|`1.1.4.242`|`c2d`|`SYSDBA`|





