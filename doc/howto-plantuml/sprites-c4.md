```plantuml
@startuml blogging-context
title Blogging Context
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
skinparam defaultTextAlignment center

!define ICONURL https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.4.0
!include ICONURL/common.puml
!include ICONURL/font-awesome-5/server.puml
!include ICONURL/font-awesome-5/gitlab.puml
!include ICONURL/font-awesome/gears.puml
!include ICONURL/font-awesome/fire.puml
!include ICONURL/font-awesome/clock_o.puml
!include ICONURL/font-awesome/lock.puml
!include ICONURL/font-awesome/cloud.puml
!include ICONURL/devicons/nginx.puml
!include ICONURL/devicons/mysql.puml
!include ICONURL/devicons/redis.puml
!include ICONURL/devicons/docker.puml
!include ICONURL/devicons/linux.puml

FA_CLOUD(internet,internet,cloud) #White {

}

DEV_LINUX(debian,Linux,node){

    Person(author, "Author")
    System(blog, "Acme Blog")

	FA_CLOCK_O(crond,crond) #White
	FA_FIRE(iptables,iptables) #White

	DEV_DOCKER(docker,docker,node)  {
		DEV_NGINX(nginx,nginx,node) #White
		DEV_MYSQL(mysql,mysql,node) #White
		DEV_REDIS(redis,redis,node) #White
		FA5_SERVER(nexus,nexus3,node) #White
		FA5_GITLAB(gitlab,gitlab,node) #White
		FA_GEARS(gitlabci,gitlab-ci-runner,node) #White

		FA_LOCK(letsencrypt,letsencrypt-client,node) #White
	}
}



Rel(author, blog, "Create Post")
Rel(author, blog, "Update Post", "https")

Person_Ext(reader, "Reader")
Rel(reader, blog, "Read Post", "https")
Rel(reader, blog, "Search Post", "https")
Rel(blog, reader, "Notify New Post","https")

@enduml
```