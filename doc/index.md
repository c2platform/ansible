# C2D - C2 Platform Automation Development Environment

## Reverse Proxy

- [Create the Reverse Proxy and Web Proxy | C2 Platform](https://c2platform.org/docs/howto/rws/dev-environment/setup/rproxy1/)


| Service                 | Node          | Link                            |
|-------------------------|---------------|---------------------------------|
| Reverse / Forward Proxy | `c2d-rproxy1` | https://c2platform.org/is-alive |
| DNSMasque               | `c2d-rproxy1` |                                 |


## Argo CD

| Service              | Node         | Link             | Reverse Proxy                            |
|----------------------|--------------|------------------|------------------------------------------|
| Kubernetes Dashboard | `okd-argocd` |                  | https://dashboard-argocd.c2platform.org/ |
| UI / API             | `okd-argocd` |                  | https://argocd.c2platform.org/           |
| Repo Server          | `okd-argocd` |                  |                                          |
| Dex Server           | `okd-argocd` |                  |                                          |
| Argo Rollouts Demo   | `okd-argocd` | http://1.1.4.25/ |                                          |

- [Manage Argo CD using Ansible | C2 Platform](http://c2platform.org/docs/howto/c2/argocd/)
