# C2 Platform Ansible Project

[![pipeline status](https://gitlab.com/c2platform/ansible/badges/master/pipeline.svg)](https://gitlab.com/c2platform/ansible/-/commits/master)
[![Latest Release](https://gitlab.com/c2platform/ansible/-/badges/release.svg)](https://gitlab.com/c2platform/ansible/-/releases)

The C2 Ansible Inventory project is a GitLab project that serves as the
[Ansible Inventory project](https://c2platform.org/en/docs/concepts/ansible/projects) for the
[C2 Platform](https://c2platform.org).

## Purpose

This project has two primary objectives:

1. **Supporting C2 Development Environment:** The RWS C2 Platform Ansible
   Project is an integral part of the C2 development environment, serving as a
   cornerstone for the C2 Ansible collections. It provides a structured
   inventory and automation framework that streamlines the development and
   management of various Ansible-based tasks and workflows within the
   C2Platform.
1. **Open-Source Reference Implementation:** Beyond its role within RWS, this
   project also operates as an open-source reference implementation. It offers a
   valuable resource for showcasing diverse automation solutions and
   exemplifying industry best practices. By doing so, it encourages
   collaboration and knowledge sharing among the wider automation community.

In summary, the RWS GIS Platform Ansible Project plays a pivotal role in
enhancing the efficiency of RWS operations while contributing to the broader
open-source ecosystem by serving as a model for effective automation practices.


## Features

* Pre-configured inventory structure: The project includes a well-organized
  inventory structure with pre-defined host and group configurations, making it
  easier to manage and scale the Ansible deployments.
* Group variables and vault files: The project incorporates group variables and
  vault files to securely store sensitive data, ensuring that confidential
  information remains protected during provisioning and deployment.
* Extensive Ansible collections and roles: The project demonstrates a wide range
  of Ansible collections and roles specifically developed for the C2 Platform.
  These can be used as a reference to understand best practices and
  implementation details for various tasks.
* The project has configuration for the creation of all kinds of services using
  Ansible. For a better understanding of its scope, capabilities refer to the
  [how-to](https://c2platform.org/en/docs/howto/c2) section.

## Getting Started

To get started with this project, please follow the
[Getting Started](https://c2platform.org/en/docs/howto/dev-environment)
guide. It provides detailed instructions on setting up the necessary prerequisites,
configuring the inventory, and deploying desired services using Ansible.

<!-- TODO contributions -->

## How-to

See [how-to](https://c2platform.org/en/docs/howto/c2) section on C2 Platform website.

## Guidelines

For software development guidelines related to this project, please refer to the
[C2 Platform Website](https://c2platform.org/en/docs/guidelines).


## License

This project is licensed under the [MIT License](./LICENSE), granting users the
freedom to use, modify, and distribute the codebase as per the license terms.

## Support

<!-- TODO test, verify -->

If you encounter any issues or have questions regarding this project, please
create an [issue](https://gitlab.com/c2platform/ansible/-/issues/service_desk)
in the GitLab repository. We will strive to address and resolve any queries or
problems as promptly as possible.
