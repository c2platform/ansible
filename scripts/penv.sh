#!/bin/bash

# Install inside vm for example c2d-xtop using source /vagrant/scripts/pyenv.sh
# Clean pyenv using rm -rf ~/ansible ~/.pyenv/ && nano ~/.bashrc

sudo apt update; sudo apt install build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev curl \
libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev -y

echo "Installing Pyenv..."
curl https://pyenv.run | bash
content_to_add='
# START pyenv.sh additions
export GIT_USER=nobody
export GIT_MAIL=nobody@example.com
export C2_VIRTUALENV=c2
export C2_LINTERS=Y
export C2_LINTERS_VIRTUALENV=$C2_VIRTUALENV
# export http_proxy=http://webproxy.c2platform.org:8080
# export https_proxy=http://webproxy.c2platform.org:8080
# export no_proxy=".c2platform.org,.local"
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
'

echo "$content_to_add" >> ~/.bashrc
echo "Content added to ~/.bashrc"

echo "Installing Python 3.10.6..."
source ~/.bashrc
pyenv install 3.10.6
pyenv global 3.10.6

echo "Creating virtual environment..."
pyenv virtualenv $C2_VIRTUALENV
pyenv virtualenvs  # this will output all virtual environments available including "rws"
pyenv virtualenv-init $C2_VIRTUALENV
pyenv activate $C2_VIRTUALENV

content_to_add='
eval "$(pyenv virtualenv-init -)"
pyenv activate $C2_VIRTUALENV
# END pyenv.sh additions
'
echo "$content_to_add" >> ~/.bashrc
echo "Content added to ~/.bashrc"

source ~/.bashrc
cd ~/
git clone https://gitlab.com/c2platform/ansible.git
cd ansible
pip3 install --upgrade pip
pip3 install -r requirements.txt
ansible-galaxy collection install -r collections/requirements.yml -p .