# How-to CodeReady Containers (CRC)

This guide will walk you through the process of setting up a local development instance of Red Hat OpenShift using [Red Hat CodeReady Containers (CRC)](https://developers.redhat.com/blog/2019/09/05/red-hat-openshift-4-on-your-laptop-introducing-red-hat-codeready-containers) on Ubuntu 22.04. his guide is based on the [Red Hat OpenShift Local Getting Started](https://developers.redhat.com/products/openshift-local/getting-started), but includes additional information specific to Ubuntu, which is not officially supported by CRC.

After completing the steps in this guide, you will be able to start your cluster by running `crc start` and the server will be accessible via the web console at https://console-openshift-console.apps-crc.testing. You can log in as either the administrator `kubeadmin` or as a developer.

To use the oc command line interface, run the following commands:

```bash
eval $(crc oc-env)
oc login -u developer https://api.crc.testing:6443
```

This guide is divided into the following sections:

- [Pull secret](#pull-secret)
- [Install / setup](#install--setup)
- [Start](#start)

## Pull secret

To obtain your "pull secret," go to https://console.redhat.com/openshift/create/local and login using your Red Hat developer account and then download your "pull secret" og in with your Red Hat developer account, and download the pull secret to your home directory, for example, `~/.pull-secret`.

<details>
  <summary>Show me</summary>

![](images/pull-secret.png)

</details>

## Install / setup

The installation and setup process involves installing some packages, downloading the `crc` binary, and then running `crc setup`.

```bash
sudo apt-get update
sudo apt-get install -y libvirt-clients libvirt-daemon libvirt-daemon-system qemu-kvm network-manager
cd Downloads
curl -LJO https://mirror.openshift.com/pub/openshift-v4/clients/crc/latest/crc-linux-amd64.tar.xz
tar -xvf crc-linux-amd64.tar.xz
sudo mv crc-linux-2.12.0-amd64/ /opt/
sudo ln -s /opt/crc-linux-2.12.0-amd64/crc /usr/local/bin/crc
chmod +x /opt/crc-linux-2.12.0-amd64/crc
```

After running the `crc setup` command for the first time, it will add the current user to the group `libvirt`. In order to complete the setup process, you will need to log out and log in again, and then run the crc setup command a second time.

```bash
crc setup
```

<details>
  <summary>Show me</summary>

```bash
user@ubuntu22:~$ crc setup
INFO Using bundle path /home/ostraaten/.crc/cache/crc_libvirt_4.11.18_amd64.crcbundle
INFO Checking if running as non-root
INFO Checking if running inside WSL2
INFO Checking if crc-admin-helper executable is cached
INFO Checking for obsolete admin-helper executable
INFO Checking if running on a supported CPU architecture
INFO Checking minimum RAM requirements
INFO Checking if crc executable symlink exists
INFO Checking if Virtualization is enabled
INFO Checking if KVM is enabled
INFO Checking if libvirt is installed
INFO Checking if user is part of libvirt group
INFO Checking if active user/process is currently part of the libvirt group
INFO Checking if libvirt daemon is running
INFO Checking if a supported libvirt version is installed
INFO Checking if crc-driver-libvirt is installed
INFO Installing crc-driver-libvirt
INFO Checking crc daemon systemd service
INFO Setting up crc daemon systemd service
INFO Checking crc daemon systemd socket units
INFO Setting up crc daemon systemd socket units
INFO Checking if AppArmor is configured
INFO Updating AppArmor configuration
INFO Using root access: Updating AppArmor configuration
INFO Using root access: Changing permissions for /etc/apparmor.d/libvirt/TEMPLATE.qemu to 644
INFO Checking if systemd-networkd is running
INFO Checking if NetworkManager is installed
INFO Checking if NetworkManager service is running
INFO Checking if dnsmasq configurations file exist for NetworkManager
INFO Checking if the systemd-resolved service is running
INFO Checking if /etc/NetworkManager/dispatcher.d/99-crc.sh exists
INFO Writing NetworkManager dispatcher file for crc
INFO Using root access: Writing NetworkManager configuration to /etc/NetworkManager/dispatcher.d/99-crc.sh
INFO Using root access: Changing permissions for /etc/NetworkManager/dispatcher.d/99-crc.sh to 755
INFO Using root access: Executing systemctl daemon-reload command
INFO Using root access: Executing systemctl reload NetworkManager
INFO Checking if libvirt 'crc' network is available
INFO Setting up libvirt 'crc' network
INFO Checking if libvirt 'crc' network is active
INFO Starting libvirt 'crc' network
INFO Checking if CRC bundle is extracted in '$HOME/.crc'
INFO Checking if /home/ostraaten/.crc/cache/crc_libvirt_4.11.18_amd64.crcbundle exists
INFO Getting bundle for the CRC executable
INFO Downloading bundle: /home/ostraaten/.crc/cache/crc_libvirt_4.11.18_amd64.crcbundle...
3.06 GiB / 3.06 GiB [-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------] 100.00% 64.35 MiB p/s
INFO Uncompressing /home/ostraaten/.crc/cache/crc_libvirt_4.11.18_amd64.crcbundle
crc.qcow2: 11.86 GiB / 11.86 GiB [------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------] 100.00%
oc: 118.18 MiB / 118.18 MiB [-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------] 100.00%
Your system is correctly setup for using CRC. Use 'crc start' to start the instance
user@ubuntu22:~$
  ```

</details>

## Start

Running start command with your pull secret will start the OpenShift "cluster" and upon completion you should be able to access OpenShift web interface using https://console-openshift-console.apps-crc.testing/. Note the output of this command, because this will output the information you need to use the OpenShift.

```bash
crc start -p ~/.pull-secret
```

Note: you only have to provide the pull secret once. After the first start, you can just use `crc start`.

<details>
  <summary>Show me</summary>

```bash
user@ubuntu22:~$ crc start -p ~/.pull-secret
INFO Checking if running as non-root
INFO Checking if running inside WSL2
INFO Checking if crc-admin-helper executable is cached
INFO Checking for obsolete admin-helper executable
INFO Checking if running on a supported CPU architecture
INFO Checking minimum RAM requirements
INFO Checking if crc executable symlink exists
INFO Checking if Virtualization is enabled
INFO Checking if KVM is enabled
INFO Checking if libvirt is installed
INFO Checking if user is part of libvirt group
INFO Checking if active user/process is currently part of the libvirt group
INFO Checking if libvirt daemon is running
INFO Checking if a supported libvirt version is installed
INFO Checking if crc-driver-libvirt is installed
INFO Checking crc daemon systemd socket units
INFO Checking if AppArmor is configured
INFO Checking if systemd-networkd is running
INFO Checking if NetworkManager is installed
INFO Checking if NetworkManager service is running
INFO Checking if dnsmasq configurations file exist for NetworkManager
INFO Checking if the systemd-resolved service is running
INFO Checking if /etc/NetworkManager/dispatcher.d/99-crc.sh exists
INFO Checking if libvirt 'crc' network is available
INFO Checking if libvirt 'crc' network is active
INFO Loading bundle: crc_libvirt_4.11.18_amd64...
INFO Creating CRC VM for openshift 4.11.18...
INFO Generating new SSH key pair...
INFO Generating new password for the kubeadmin user
INFO Starting CRC VM for openshift 4.11.18...
INFO CRC instance is running with IP 192.168.130.11
INFO CRC VM is running
INFO Updating authorized keys...
INFO Configuring shared directories
INFO Check internal and public DNS query...
INFO Check DNS query from host...
WARN Wildcard DNS resolution for apps-crc.testing does not appear to be working
INFO Verifying validity of the kubelet certificates...
INFO Starting kubelet service
INFO Waiting for kube-apiserver availability... [takes around 2min]
INFO Adding user's pull secret to the cluster...
INFO Updating SSH key to machine config resource...
INFO Waiting for user's pull secret part of instance disk...
INFO Changing the password for the kubeadmin user
INFO Updating cluster ID...
INFO Updating root CA cert to admin-kubeconfig-client-ca configmap...
INFO Starting openshift instance... [waiting for the cluster to stabilize]
INFO 2 operators are progressing: image-registry, openshift-controller-manager
INFO Operator image-registry is progressing
INFO Operator authentication is degraded
INFO All operators are available. Ensuring stability...
INFO Operators are stable (2/3)...
INFO Operators are stable (3/3)...
INFO Adding crc-admin and crc-developer contexts to kubeconfig...
Started the OpenShift cluster.

The server is accessible via web console at:
  https://console-openshift-console.apps-crc.testing

Log in as administrator:
  Username: kubeadmin
  Password: **********

Log in as user:
  Username: developer
  Password: developer

Use the 'oc' command line interface:
  $ eval $(crc oc-env)
  $ oc login -u developer https://api.crc.testing:6443
user@ubuntu22:~$
```

</details>



