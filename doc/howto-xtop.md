# How-to Remote Desktop

This instruction provides step-by-step guidance on how to create the **Ubuntu 22.04** LXD node `c2d-xtop`, and set up a **Remote Desktop Protocol** (RDP) connection using `xrdp`. This is an alternative approach to using a SOCKS proxy for accessing the development environment. The obvious advantage of this approach is of course that you can access the development environment using more than a browser. It can for example be used for Docker operations. It is not recommended to do that on the LXD host because Docker conflicts with LXD.

## Setup

To create the Ubuntu 22.04 LXD node, run the following command in your terminal:

`vagrant up c2d-xtop`

You can find additional configuration details in [xtop.yml](../plays/dev/xtop.yml) playbook and in [group_vars/xtop/main.yml](../group_vars/xtop/main.yml) configuration file.

## Configure RDP connection

To configure a new RDP connection using [Remmina](https://remmina.org/), follow these steps:

1. Install Remmina. It is in the **Ubuntu Software** repository.
1. Open Remmina, and select "New Connection" from the menu.
1. In the **Basic** tab, enter the information below.
1. Save the configuration.

|Property|Value      |
|--------|-----------|
|Server  |`1.1.4.162`|
|Username|`vagrant`  |
|Password|`vagrant`  |

## Verify

You should now be able to create a remote desktop using **Remmina**. If you encounter any issues, double-check your configuration settings and ensure that your LXD node is running correctly.

## Import root CA

Start **FireFox** and import the root CA certificate `.ca/c2.crt`. See [How-to Socks Proxy](./howto-socks-proxy.md) for more information.

## Docker-in-docker

This node can for example be used to troubleshoot / experiment with Docker-in docker ( dind ). A Docker-in-docker setup is provisioned in this node on default see [group_vars/xtop/main.yml](../group_vars/xtop/main.yml) configuration file.

* [Docker-in-docker ( dind )](./howto-dind.md)