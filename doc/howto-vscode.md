# How-to Visual Studio Code

- [Extensies](#extensies)
- [Ansible VS Code Extension by Red Hat](#ansible-vs-code-extension-by-red-hat)
- [Multiple Cursor Selection](#multiple-cursor-selection)
- [Font size](#font-size)
- [Comment / uncomment](#comment--uncomment)

## Extensies

1. C2 Platform Ansible Snippets
2. GitLens
3. Pylance
4. Python
5. YAML
6. Trailing Spaces ( Shardul Mahadik )
7. Markdown All in One ( Yu Zhang )
8. Ansible VS Code Extension by Red Hat
9. Markmap

Review

1. GitLab Workflow
2. Reveal
3. Reveal file in Folder ( Molunerfinn )
4. VSCode snippets for Ansible

## Ansible VS Code Extension by Red Hat

`Ctrl-Shift-P` and then select **Open Workspace Settings**.

```json
	"settings": {
		"ansible.python.activationScript": "~/git/gitlab/c2/ansible-dev/doc/howto-vscode/ansible_python_activation.sh",
		"ansible.ansible.path": "~/.virtualenv/c2d/bin/ansible",
		"ansible.ansibleNavigator.path": "~/.virtualenv/c2d/bin/ansible-navigator",
		"ansible.ansibleLint.enabled": true,
		"ansible.ansibleLint.arguments": "",
		"ansible.ansibleLint.path": "~/.virtualenv/c2d/bin/ansible-lint",
		"ansible.executionEnvironment.containerEngine": "docker",
		"ansible.executionEnvironment.enabled": false,
		"ansible.python.interpreterPath": "",
		"ansibleServer.trace.server": "verbose",
		"ansible.validation.enabled": true,
		"ansible.validation.lint.enabled": true,
		"ansible.validation.lint.path": "~/.virtualenv/c2d/bin/ansible-lint"
	}
```

## Multiple Cursor Selection

- <kbd>Alt</kbd> + <kbd>Click</kbd>:  Create a new cursor at the location of your click.
- <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Up/Down</kbd>: Create a new cursor above or below the current cursor.
- <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>L</kbd>: Create multiple cursors at all occurrences of the current selected text.
- <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Shift</kbd> + <kbd>I</kbd>: Create multiple cursors at the beginning of each line of the selected text.
- <kbd>Ctrl</kbd> + <kbd>D</kbd>: Select the next occurrence of the current selected text and create a cursor. Repeat to continue selecting next occurrences.
- <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>Click</kbd>: Add another cursor at the location of your click, allowing you to edit multiple places at once.

You can also use the command palette (<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>P</kbd>) and type "Create cursor" to see all the options available.
Please note that the above shortcuts are based on Windows and Linux, If you are using Mac, please use cmd instead of ctrl

## Font size

<kbd>Cntrl</kbd> <kbd>Shift</kbd> <kbd>+</kbd> and <kbd>Cntrl</kbd> <kbd>-</kbd>.

## Comment / uncomment

<kbd>Cntrl</kbd> <kbd>\\</kbd>
