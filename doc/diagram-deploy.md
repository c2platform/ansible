

```plantuml
@startuml
rectangle Internet {
    node galaxy.ansible.com
}
rectangle dev_laptop <<Ubuntu 22.04 LTS>> {
    actor engineer
    node c2d_rproxy1 <<lxd>> {
        component rproxy1 <<Apache2>> {
            component awx.c2platform.org <<VirtualHost>>
            component winapp_tomcat.c2platform.org <<VirtualHost>>
            component winapp_iis.c2platform.org <<VirtualHost>>
        }
    }
    node c2d_awx <<virtualbox>> {
        component awx_web <<container>>
        component awx_task <<container>>
    }
    node c2d_iis <<virtualbox>> {
        together {
            component index.html <<website>>
            component winapp_service.html <<website>>
        }
        component winapp <<service>>
    }
    node c2d_tomcat1 <<lxd>> {
        component tomcat1 <<tomcat service>> {
            component helloworld <<webapp>>
        }
    }
    node c2d_db1 <<lxd>> {
        component postgresql <<service>> {
            component helloworld <<webapp>>
        }
    }
    component "Browser" {
        component AWX_interface
        component galaxy_interface
        component winapp_interface
        component winapp_service_interface
    }
    component vs_code
    engineer --> vs_code
    engineer --> AWX_interface
    engineer --> galaxy_interface
    galaxy_interface --> galaxy.ansible.com
    AWX_interface --> awx.c2platform.org
    awx.c2platform.org --> awx_web
    awx_web --> awx_task
    awx_task --> galaxy.ansible.com

    engineer --> winapp_interface
    engineer --> winapp_service_interface
    winapp_interface --> winapp_tomcat.c2platform.org
    winapp_service_interface --> winapp_iis.c2platform.org
    winapp_iis.c2platform.org --> index.html
    winapp_iis.c2platform.org --> winapp_service.html
    winapp_service.html -- winapp
    winapp_tomcat.c2platform.org --> helloworld
    helloworld --[hidden]> postgresql
}
@enduml
```