# How-To GitLab in OpenShift

Install GitLab in OpenShift using [GitLab Operator](https://docs.gitlab.com/operator/).

## Links

* [GitLab Operator | GitLab](https://docs.gitlab.com/operator/)

## Prerequisites

* [How-to CodeReady Containers (CRC)](./howto-crc.md)


## Installation

[GitLab Operator](https://docs.gitlab.com/operator/) → [Installation | GitLab](https://docs.gitlab.com/operator/installation.html).

> not yet suitable for production

### Ingress ( optional )

### cert-manager

Via **OperatorHub** install [cert-manager](https://console-openshift-console.apps-crc.testing/operatorhub/ns/gitlab-system?keyword=cert-ma&details-item=cert-manager-community-operators-openshift-marketplace)

1.11.0



https://docs.gitlab.com/operator/installation.html#cluster




Note: links to [OperatorHub](https://console-openshift-console.apps-crc.testing/operatorhub/ns/gitlab-system?keyword=gitlab&details-item=gitlab-operator-kubernetes-community-operators-openshift-marketplace

Custom GitLab Runner image

## IngressClass

See [OperatorHub](https://console-openshift-console.apps-crc.testing/operatorhub/ns/gitlab-system?keyword=gitlab&details-item=gitlab-operator-kubernetes-community-operators-openshift-marketplace)

> Cluster-wide IngressClass should be created prior to Operator setup, as OLM does not currently support this object type:

```yaml
apiVersion: networking.k8s.io/v1
kind: IngressClass
metadata:
  # Ensure this value matches `spec.chart.values.global.ingress.class`
  # in the GitLab CR on the next step.
  name: gitlab-nginx
spec:
  controller: k8s.io/ingress-nginx
```